  #{
  prerecs:
  ensure have instrument-control installed
#}

pkg load instrument-control;

function [char_array] = ReadToTerm (srlvar)
  term_char = 13;
  noterm = true;
  i=1;
  int_array = uint8(1);


  #following part: while w/o term char read a single char from stream, add to array
  while noterm
    thischar = srl_read(srlvar, 1);
    #check for term, where \r is 13 & \n is 10
    if(thischar == 13)
      #printf("have r term at index position %d\n",i);
      noterm = false;
    elseif(thischar == 10)
      #printf("have n term at index position %d\n",i);
      noterm=false;
    else
      int_array(i) = thischar;
      i=i+1;
    endif
  endwhile
  #when done convert int array to char array and return it
  char_array = char(int_array);
endfunction

#read and print line from serial if not empty
function [x,y,z,t] = readline (srlvar)
  noreturn=true;
  #x=0;
  #y=0;
  #z=0;
  #t=0;
  this_str = ReadToTerm(srlvar);
  if(length(this_str) != 1)
    #trim string
    #this_str=strtrim(this_str);

    #format: x,y,z,milli
    coord=strsplit(this_str,",");
    #printf("coord length is %d\n",length(coord));

    if(length(coord)==4)
      x=str2double(coord{1});
      y=str2double(coord{2});
      z=str2double(coord{3});
      t=str2double(coord{4})/1000;
      noreturn=false;
    endif


    #{
    for(i=1:length(coord))
      printf("coord[%d] is: %s\n",i,coord{i});
    endfor
    #}

    #{
    #parse orientation & calibration data
    orient_str="Orientation: ";
    #printf("this_str is '%s'\n",this_str);
    orient_i = index(this_str,orient_str);
    orient_r = rindex(this_str,orient_str);
    #printf("length of orient_str is %d\n",length(orient_str));

    #calib_i = index(this_str,"Calibration: ");
    #printf("\ncalib_i is %d & orient_i is %d\n",calib_i,orient_i);
    #printf("orient_i is %d\n",orient_i);

    #if orient line AND only 1 occurrence of orient to account for probably arduino-code-bug (latter clause)
    if( (orient_i == 1) && (orient_r == orient_i) )

      #print substring output
      coords_str = substr(this_str,length(orient_str)+1);
      #printf("coords: %s\n",coords_str);

      #now get x & y
      spaces = strfind(coords_str," ");
      #printf("spaces data is %d\n",spaces);
      #printf("spaces(1) is %d\n",spaces(1));

      #get coords in str format
      xs=substr(coords_str,1,spaces(1)-1);
      ys=substr(coords_str,spaces(1)+1,spaces(2)-spaces(1)-1);
      zs=substr(coords_str,spaces(2)+1,length(coords_str)-spaces(2)-1);

      #get coords in number-form, thereafter assign x/y/z to these
      xd=str2double(xs);
      yd=str2double(ys);
      zd=str2double(zs);

      #printf("xs vartype is %s\n",typeinfo(xs));

      x=xd;
      y=yd;
      z=zd;

      #printf(" in fn - x: %s, y: %s, z: %s\n",x,y,z);
      #}
      #coords=[x;y;z];
      #noreturn=false;

      #print raw fn output
      #printf("%s",this_str);

      #following block testing re getting blank/contentless strings (solved w size of 1 above)
      #printf("\nascii representation of string is: ");
      #disp(toascii(this_str));
      #disp("string disp is: "), disp(this_str);
      #ascii returns "1" for empty string from function
      #printf("str length is %d\n",length(this_str));
      #{
      if(strcmp(this_str,"\n"))
        printf(" - STRING IS NEWLINE n ONLY");
      elseif(strcmp(this_str,"\r"))
        printf(" - STRING IS NEWLINE r ONLY");
      elseif(isempty(this_str))
        printf(" - STRING empty by empty function");
      #elseif(this_str == "")
      #  printf(" - STRING empty by empty quotes");
      else
        printf(" - string NOT newline/empty");
      endif
      #}
      #printf("\n");
    #endif
  endif
  if(noreturn == true)
    #printf("no return state\n");
    #send blank vars back to calling code, for it to ignore
    x="";
    y="";
    z="";
    t="";
  endif
endfunction

function plot_fft(d,Hz)
  n=length(d);
  this_fft=fft(d,n);
  freq_range = (0:n-1)*(Hz/n);

  printf("doing fft now w vars: n=%d\n",n);

  power = abs(this_fft).^2/n;    % power of the DFT
  subplot(2,1,2);
  plot(freq_range,power)
  #title ("wubbalubbadubdub POWER FFT");
  #xlabel('Frequency')
  #ylabel('Power')
#{
  magnitudeY = abs(this_fft);        % Magnitude of the FFT
  phaseY = unwrap(angle(this_fft));  % Phase of the FFT
  #helperFrequencyAnalysisPlot1(freq_range,magnitudeY,phaseY,Hz);
  plot(fft(d));
  #}
endfunction

function setup3dplot()
  hold off;
  #for 3d plot
  #plot3(1,1,1);
  title ("wubbalubbadubdub");
  xlabel ("t on x axis");
  ylabel ("xyz on y axis");
  zlabel ("x on z axis");
  axis("auto x");
  axis("auto y");
  axis("auto z");
  hold on;
endfunction

function do3dplot(t,x,y,z)
  #3d plot that works
  plot3(t,y,x);
  drawnow();
endfunction

function setupplot()
  hold off;

  subplot(2,1,2);
  title ("wubbalubbadubdub POWER FFT");
  xlabel('Frequency')
  ylabel('Power')

  subplot(2,1,1);
  title ("wubbalubbadubdub");
  xlabel ("t on x axis");
  ylabel ("xyz on y axis");
  axis("auto x");
  axis("auto y");
  legend("x","y","z");
  hold on;
endfunction

function doplot (t,x,y,z,plotrun)
  #multiple lines on same 2d plot - works
  subplot(2,1,1);
  #works
  plot(t,y,t,x,t,z);

  #plot(t,x,"displayname","x");
  #plot(t,y,"displayname","y");
  #plot(t,z,"displayname","z");
  if(plotrun==false)
    setupplot();
  endif
  drawnow();
endfunction

#estimate sampling freq in Hz taking vector array "t" of time points & "s" # of samples to use for calculation
function sfreq=est_sample_freq(t,s)
  #generalized freq fn(s,tlen) = 1 / avg sampling rate
  #avg sampling rate avgs = loop{k=0 to k=(s-2)} [( t(tlen-k) - t(tlen-k+1)] / (s-1)
    #numerator = ssum = sampling interval sum
  ssum=0;
  #ssum = for loop from k=0 to k=s-2
  #add in each for loop to ssum: t(tlen-k) - t(tlen-(k+1))
  tlen=(length(t));
  for(k=0:s-2)
     ssum = ssum + ( t(tlen-k) - t(tlen-(k+1)) );
  endfor
  avgs=ssum/(s-1);
  sfreq=1/avgs;

  #this is wrong: using last s num of samples & i=tlen, sampling rate should be: srate= s / [t(i) - t(i-s+1)]
  #s=20;

  #{
  ti=t(tlen);
  tj=t(tlen-s+1);
  tdelta=ti-tj;
  sfreq=s/tdelta
  ss=tdelta/s
  #}

  printf("sample fn avgs: rate=%d Hz (sampling avg: ~%ds; # samples: %d; tlen: %d\n",sfreq,avgs,s,tlen);
  #tlen=%d,s=%d,td=%d from ti=%d & tj=%d)\n,sfreq,avgs,tlen,s,tdelta,ti,tj);

endfunction

function doit(srlvar)
  #read from serial loop
  x=[];
  y=[];
  z=[];
  t=[];

  #iteration number, iteration w data aquision, clock start time, set current time to 0 before loop, sampling rate
  i=0;
  j=0;
  t0=clock();
  ti=t0;
  si=0;

  #setup plot type, labels, axes
  #setupplot();
  plotrun=false;


  printf("starting main loop...\n");

  while true
    #try to get recent coords from readline fn, then process only if non-blank vars returned
    [xi,yi,zi,ti] = readline(srlvar);
    if(!isempty(xi))
      #update ti=last elapsed time since start
      #ti=etime(clock(),t0);
      sampl_str="(acquiring...)";
      if(si!=0)
        sampl_str=[num2str(si) " Hz"];
      endif
      printf("coords [ x=%d,  y=%d,  z=%d ]  t=%ds sampl=%s\n",xi,yi,zi,ti,sampl_str);
      fflush(stdout);
      #assigned returned coord vars to their respective vectors/arrays
      t=[t;ti];
      y=[y;yi];
      x=[x;xi];
      z=[z;zi];

      #disp("x is: ");
      #disp(x);
      #printf("xi var type is %s, x var type is %s\n",typeinfo(xi),typeinfo(x));
      #doplot(ti,xi,yi,zi);

      #plot the variables using plot function above - functional
      doplot(t,x,y,z,plotrun);

      #plot(str2num(ti),str2num(xi));
      #data = srl_read(s1, 12);
      #read_str = char(data);
      #printf("%s",read_str);


      if(j>50 && mod(j,100) == 0)
        si=est_sample_freq(t,20);

        #will take fft_n samples: so from [array length - (array length=1)] to (array length)
        #num samples to use for fft calc
        fft_n=50;
        #get length of vector for fft
        xlen=length(x);
        #subset of x array -> pass to plot_fft w sampling rate
        x_sub=x((xlen-fft_n+1):xlen);
        sublen=length(x_sub);

        printf("sampling rate check at j=%d, t=%d, xlen=%d, sublen=%d\n",j,ti,xlen,sublen);

        #plot_fft(x_sub,si);
      endif

      j=j+1;
    endif
    i=i+1;
    #pausing totally fucks up communication - interfering w baud rate read line?
    #pause(0.05);
  endwhile
endfunction



printf("code init\n");

if (exist("serial") != 3)
    disp("No Serial Support");
else
  fflush(stdout);

  s1 = serial("/dev/ttyUSB0", 115200);
  pause(2);

  set(s1, 'bytesize', 8);
  set(s1, 'parity', 'n');
  set(s1, 'stopbits', 1);
  srl_flush(s1);

  doit(s1);
  fclose(s1);
endif
