# tremor analysis

This is a project aimed at developing free and open source tools for tremor analysis. Initial development work in early July 2018 employs the Octave software package to interface with an arduino device connected to a position sensor.

Use overview:
* setup arduino:
  * install arduino IDE
  * connect motion sensor to arduino
  * test in arduino IDE with arduino_tremor_test.ino
* setup Octave
  * install Octave
  * install/configure serial communication package and test this
  * test arduino to octave communication & position sensing/parsing using tremor_analysis.m file

file overview:
* arduino file: arduino tremor test / arduino_tremor_test.ino
* octave file: tremor_analysis.m
* license file
* signing-keys file

## hardware setup
* arduino unit used for testing: metro mini
* following motion sensors have been tested re basic functionality w arduino/octave communication
  * BNO055 = a 9-DOF sensor
    * https://learn.adafruit.com/adafruit-bno055-absolute-orientation-sensor/overview
  * MPU6050 = a 6-DOF sensor
* connect motion sensors as per manufacturer instructions (see adafruit page for e.g. circuit diagrams for BNO055)
* test the arduino/motion sensor code before using Octave - monitor the output in the serial console window. make sure to close serial console window before using octave

## software setup
* note that testing has been performed using Arch Linux and Fedora, though no  reason why should not work on other systems, including OSX or Windows

### install and setup Arduino IDE
* there are many guides on the net for this and should take less than 1 min on almost all systems/distributions
* test the `arduino_tremor_test.ino` file
  * monitor serial console in arduino IDE to ensure human & Octave readable output
* make sure to close serial console before using Octave

### setup octave
* download/install the Octave package (details dependent on operating system)
* install instrument control package for Octave:
  * download from https://octave.sourceforge.io/instrument-control/
  * under "file browser" window navigate to folder containing the downloaded file
  * in command prompt window (cmd) run (2nd line tries to load package):
    * `pkg install instrument-control-0.3.1.tar.gz`
    * `pkg load instrument-control`

### setup arduino serial connection
* connect the arduino device
* determine what port the arduino is connected to (system-dependent)
  * one more system-independent method is to use the arduino IDE and find the port in one of the menus
  * for arch linux:
    * `run journalctl -xef`
    * insert arduino
    * look for port/tty, e.g. ttyUSB0 from:
    `[date stamp] [system name] kernel: usb 1-2: cp210x converter now attached to ***ttyUSB0***`
  * note you must ensure arduino IDE serial connection window is closed prior to attempting to use Octave to connect to arduino (otherwise the arduino IDE may block the Octave serial connection)
* now let us test Octave with serial connections - put the folloing in Octave cmd:

```
if (exist("serial") == 3)
  disp("Serial: Supported")
else
  disp("Serial: Unsupported")
endif
```

* test connecting to specific arduino port w baud rate of 115200 (change the `/dev/ttyUSB0` to your port):

`s1 = serial("/dev/ttyUSB0", 115200)`

* if issues check serial communication parameters (this may be necessary)
  * check arduino preferences (a submenu item in arduino IDE that should refer to a preferences.txt file)

```
parity: N
byte size ("databits"?): 8
stop bits: 1
```

### octave testing
* run the tremor_analysis.m octave file
