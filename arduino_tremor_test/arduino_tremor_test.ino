#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

unsigned long previousTime = 0;
//long interval = 10;

//sampling frequency in Hz
#define SAMPLE_FREQ (20);
unsigned const int SAMPLING_RATE_MS = 1000/SAMPLE_FREQ;

Adafruit_BNO055 bno = Adafruit_BNO055();

void setup() {
  Serial.begin(115200);
  delay(100);

   /* Initialise the sensor */
  Serial.print("initializing...");
  if(!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while(1);
  }
  else Serial.print("sensor detected ok");
  delay(1000);
  
  /* Display the current temperature */
  int8_t temp = bno.getTemp();
  Serial.print("Current Temperature: ");
  Serial.print(temp);
  Serial.println(" C");
  Serial.println("");
  
  bno.setExtCrystalUse(true);

  /*
  Serial.println("Calibration status values: 0=uncalibrated, 3=fully calibrated");

  uint8_t system, gyro, accel, mag = 0;
  bno.getCalibration(&system, &gyro, &accel, &mag);
  Serial.println("starting calibration loop...");
  while(accel < 3){
    bno.getCalibration(&system, &gyro, &accel, &mag);
    Serial.println(accel, DEC);
    delay(100);
  }
  */
}

void loop() {
  // put your main code here, to run repeatedly:
    // Possible vector values can be:
  // - VECTOR_ACCELEROMETER - m/s^2
  // - VECTOR_MAGNETOMETER  - uT
  // - VECTOR_GYROSCOPE     - rad/s
  // - VECTOR_EULER         - degrees
  // - VECTOR_LINEARACCEL   - m/s^2
  // - VECTOR_GRAVITY       - m/s^2

  unsigned long currentTime = millis();
  if (currentTime - previousTime >= SAMPLING_RATE_MS){
    imu::Vector<3> acc = bno.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);
    //imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
  
    Serial.print(acc.x());
    Serial.print(",");
    Serial.print(acc.y());
    Serial.print(",");
    Serial.print(acc.z());
    Serial.print(",");
    Serial.print(millis());
    //Serial.print(",");
    //uint8_t system, gyro, accel, mag = 0;
    //bno.getCalibration(&system, &gyro, &accel, &mag);
    //Serial.print(system, DEC);
    //Serial.print(",");
    //Serial.print(gyro, DEC);
    //Serial.print(",");
    //Serial.print(accel, DEC);
    //Serial.print(",");
    //Serial.print(mag, DEC);
    Serial.println();
    previousTime = currentTime;

/*
  // Display the floating point data
  Serial.print("X: ");
  Serial.print(euler.x());
  Serial.print(" Y: ");
  Serial.print(euler.y());
  Serial.print(" Z: ");
  Serial.print(euler.z());
  Serial.print("\t\t");

  
  // Quaternion data
  imu::Quaternion quat = bno.getQuat();
  Serial.print("qW: ");
  Serial.print(quat.w(), 4);
  Serial.print(" qX: ");
  Serial.print(quat.y(), 4);
  Serial.print(" qY: ");
  Serial.print(quat.x(), 4);
  Serial.print(" qZ: ");
  Serial.print(quat.z(), 4);
  Serial.print("\t\t");
 
  //Display calibration status for each sensor.
  uint8_t system, gyro, accel, mag = 0;
  bno.getCalibration(&system, &gyro, &accel, &mag);
  Serial.print("CALIBRATION: Sys=");
  Serial.print(system, DEC);
  Serial.print(" Gyro=");
  Serial.print(gyro, DEC);
  Serial.print(" Accel=");
  Serial.print(accel, DEC);
  Serial.print(" Mag=");
  Serial.println(mag, DEC);
  */
  }

}
